FROM registry.gitlab.com/ulrichschreiner/docker-hugo:latest as builder

RUN chmod -R 777 /usr/share/blog && hugo

FROM  bitnami/nginx:latest

COPY --from=builder /usr/share/blog/public /app
